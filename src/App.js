import { useState } from 'react';


function SelectRandomNumber({value}){
  useState(value);
}


function App(){
  const [randomState, setRandomState] = useState(1);
  const number = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  const listItems = number.map((n) => <option value={n}>{n}</option>);
  const [isSending, setSending] = useState(false);
  const [response, setResponse] = useState([])
  function handleRandomGenerator(e){
    e.preventDefault();
    setSending(true);
    fetch("https://v8atwz0g24.execute-api.us-east-2.amazonaws.com/default/ServerlessGermy?n="+ randomState).then(response =>{return response.json()}).then(data => {setResponse(data); setSending(false);}); 

  }
  
  return (
    <div className="Random Number Generator">
      <header className="Randon-header">
        <h1>Random Number Generator</h1>
        <form  onSubmit={handleRandomGenerator}>
          <label>
          <select value = {randomState} onChange={(e)=> setRandomState(e.target.value)} disabled={isSending}>
            {listItems}
          </select>
          </label>
          <input type="submit" disabled={isSending} value="Randomize"/>
        </form>
        <p>{response.map(n => <div>{n}</div>)}</p>
      </header>
    </div>
  );

}


export default App;
